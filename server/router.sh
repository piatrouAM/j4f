function server_router {

    route=$(echo $1 | sed 's/\///g')

    if [[ -e $routes_path/$route.sh ]]
    then
        echo -e "HTTP/1.1 200 OK\n\n"
        source $routes_path/$route.sh
    else
        echo -e "HTTP/1.1 404 NOT FOUND\n\n"
    fi
}