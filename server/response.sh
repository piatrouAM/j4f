function server_response(){
    read req_head
    if echo $req_head | grep -qE '^GET '
    then
        req_method='GET'
        req_uri=$(echo $req_head | awk '{ print $2 }') 
    fi

    server_router $req_uri
    # echo -e "HTTP/1.1 200 OK\n\n$req_uri"
}