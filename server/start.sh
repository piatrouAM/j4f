function server_start(){
    rm -f pipe$1
    mkfifo pipe$1
    trap "rm -f pipe$1" EXIT


    while true 
    do
        cat pipe$1 | nc -l -p $2 > >(
            server_response > pipe$1
        )
    done
}